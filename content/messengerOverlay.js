/*
 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

"use strict";

var TinyLocales = {
  getPrefSvc: function() {
    let service = Cc["@mozilla.org/preferences-service;1"].getService(Ci.nsIPrefService);
    return service.getBranch(null);
  },

  getCurrentLocale: function() {
    let prefRoot = this.getPrefSvc();
    let locale = null;
    try {
      locale = prefRoot.getCharPref("intl.locale.requested");
    }
    catch (ex) {}
    if (!locale) locale = "en-US";

    return locale;
  },

  setLocale: function(locale) {
    let prefRoot = this.getPrefSvc();
    prefRoot.setCharPref("intl.locale.requested", locale);
  },

  changeLocale: function() {
    let promptSvc = Components.classes["@mozilla.org/embedcomp/prompt-service;1"].getService(Components.interfaces.nsIPromptService);
    let check = {
      value: false
    };
    let input = {
      value: this.getCurrentLocale()
    };

    let result = promptSvc.prompt(window, "Enter Locale", "Please type the new locale", input, null, check);
    if (!result) return;

    this.setLocale(input.value);

    // restart Thunderbird
    let appStartup = Components.classes["@mozilla.org/toolkit/app-startup;1"].getService(Components.interfaces.nsIAppStartup);
    appStartup.quit(appStartup.eAttemptQuit | appStartup.eRestart);

  }
}
